# Caves

A Platformer in JS... Work in progress

![A Random World](screenshot/screen1.png)

## Status
For the moment, only the game engine (physics and player action) is partially implemented.
The play level is currently a simple random generation just useful for testing the engine.

## Command
- Left arrow and right arrow to go... to the left or to the right
- Up arrow to jump
- w to take an object

## GFX
Currently, I use the work of [0x72](https://0x72.itch.io/8x8-f24-tileset) and [Fantasy24](https://lospec.com/palette-list/fantasy-24) color palette.

## Todo
- [ ] Enlarge world map
    - [X] Camera system
    - [X] Scrolling
    - [X] Limit drawing tile in screen view
- [ ] Improve the quality of random map
- [ ] Add tools for interaction
- [ ] Add use item possibility
...