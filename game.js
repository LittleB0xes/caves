let canvas = document.getElementById("game");
let ctx = canvas.getContext('2d');
const cellSize = 32;
const viewWidth = 36; // 40
const viewHeight = 18; //20
const worldWidth = 200;
const worldHeight = 200;

canvas.width = viewWidth  * cellSize;
canvas.height = viewHeight * cellSize;

ctx.textBaseline = 'top';
ctx.font = '16px bit';

let frame_count = 0;
let tileSet = new Image();


let player;
let world;

ctx.imageSmoothingEnabled = false; // Love pixel !! Hate Smoothing


window.addEventListener("keydown", keyDown, false);
window.addEventListener("keyup", keyUp, false);


function init() {
	world = new World(worldWidth, worldHeight);
	player = new Player(10 * cellSize, 10 * cellSize,EntityType.PLAYER);
	camera = new Camera(0, 0, worldWidth * cellSize, worldHeight * cellSize);

	tileSet.onload = function() {
				window.requestAnimationFrame(mainLoop);
	};
	tileSet.src = "Assets/game_tileset.png";	
}

function mainLoop() {
	ctx.fillStyle = 'rgb(21, 15, 10)';
	ctx.fillRect(0,0, canvas.width, canvas.height);
	player.update(world.levelOne[1]);
	camera.update(player);
	world_interaction(player, world.levelOne);
	entityCleaner(world.levelOne);
	render(frame_count, camera, player, world.levelOne);

		
	frame_count += 1;
	requestAnimationFrame(mainLoop);
}



function keyDown(e) {
	switch(e.key) {
		case "ArrowLeft" :
			player.goingLeft(true);
		break;
		case "ArrowRight":
			player.goingRight(true);
		break;
		case "ArrowUp":
			player.jumping(true);
		break;
		case "w":
			player.bring = true;
		break;
	}

}

function keyUp(e) {
    switch(e.key) {
		case "ArrowLeft" :
			player.goingLeft(false);
		break;
		case "ArrowRight":
			player.goingRight(false);
		break;
		case "ArrowUp":
			player.jumping(false);
		break;
		case "w":
			player.bring = false;
		break;
	}
}

// window.onload  = init
init();