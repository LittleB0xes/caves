const SpriteInfo = {
    PlayerGoLeft: {
        x: 0,
        y: 8,
        w: 8,
        h: 8,
        frame: 4,
    },
    PlayerGoRight: {
        x: 0,
        y: 0,
        w: 8,
        h: 8,
        frame: 4,
    },
    PlayerJumpLeft: {
        x: 16,
        y: 40,
        w: 8,
        h: 8,
        frame: 2,
    },
    PlayerJumpRight: {
        x: 0,
        y: 40,
        w: 8,
        h: 8,
        frame: 2,
    },

    PlayerIdleLeft: {
        x: 0,
        y: 24,
        w: 8,
        h: 8,
        frame: 4,
    },
    PlayerIdleRight: {
        x: 0,
        y: 16,
        w: 8,
        h: 8,
        frame: 4,
    },
    GreenGroundLeftCorner: {
        x: 0,
        y: 48,
        w: 8,
        h: 8,
        frame: 1,
    },
    GreenGroundRightCorner: {
        x: 16,
        y: 48,
        w: 8,
        h: 8,
        frame: 1,
    },
    GreenGroundDownLeftCorner: {
        x: 8,
        y: 64,
        w: 8,
        h: 8,
        frame: 1,
    },
    GreenGroundDownRightCorner: {
        x: 16,
        y: 64,
        w: 8,
        h: 8,
        frame: 1,
    },
    GreenGroundDownMiddle: {
        x: 8,
        y: 64,
        w: 8,
        h: 8,
        frame: 1,
    },
    GreenGroundLeftMiddle: {
        x: 0,
        y: 56,
        w: 8,
        h: 8,
        frame: 1,
    },

    GreenGroundRightMiddle: {
        x: 16,
        y: 56,
        w: 8,
        h: 8,
        frame: 1,
    },

    GreenGroundMiddle: {
        x: 8,
        y: 48,
        w: 8,
        h: 8,
        frame: 1,
    },
    BlueFlower: {
        x: 32,
        y: 8,
        w: 8,
        h: 8,
        frame: 1,
    },
    WhiteFlower: {
        x: 32,
        y: 16,
        w: 8,
        h: 8,
        frame: 1,
    },
    GrassOne: {
        x: 32,
        y: 24,
        w: 8,
        h: 8,
        frame: 1,
    },
    GrassTwo: {
        x: 32,
        y: 32,
        w: 8,
        h: 8,
        frame: 1,
    },
    GrassThree: {
        x: 32,
        y: 40,
        w: 8,
        h: 8,
        frame: 1,
    },
    RedFlower: {
        x: 32,
        y: 48,
        w: 8,
        h: 8,
        frame: 1,
    },
    GoldCoin: {
        x: 32,
        y: 0,
        w: 8,
        h: 8,
        frame: 4,
    },

    RedMushroom: {
        x: 32,
        y: 56, 
        w: 8,
        h: 8,
        frame: 1,
    }

};

class Camera {
    constructor(x, y, xmax, ymax) {
        this.x = x;
        this.y = y;
        this.maxX = xmax;
        this.maxY = ymax;
    }
    update(p) {
        let cameraOffsetX = 200;
        let cameraOffsetY = 100;
        if (p.y > this.y + viewHeight * cellSize - cameraOffsetY) {
            this.y += Math.abs(p.vy);
        } else if (p.y < this.y + cameraOffsetY) {
            this.y -= Math.abs(p.vy);
        }

        if (p.x < this.x + cameraOffsetX) {
            this.x -= Math.abs(p.vx);
        } else if (p.x > this.x +viewWidth * cellSize - cameraOffsetX) {
            this.x += Math.abs(p.vx);
        }
    }   
}

function render(frame_count, c, p, viewMap) {

    // Player
    frame = Math.floor((frame_count / 8 ) % p.sprite.frame);
    ctx.drawImage(
        tileSet,
        p.getSprite().x  + frame * 8,
        p.getSprite().y,
        p.getSprite().w,
        p.getSprite().h,
        p.getX() - c.x,
        p.getY() - c.y,
        32,32
    );

    // Background

    //Item
    for (let item of viewMap[2]) {
        if (item.active
            && item.x *32 > c.x - 32 && item.x * 32 < c.x + viewWidth * 32 
            && item.y * 32 > c.y - 32 && item.y * 32 < c.y + viewHeight * 32)  {
            frame = Math.floor((frame_count / 8 ) % item.sprite.frame);
            ctx.drawImage(
                tileSet,
                item.getSprite().x  + frame * 8,
                item.getSprite().y,
                item.getSprite().w,
                item.getSprite().h,
                item.getX() * 32 - c.x,
                item.getY() * 32 - c.y,
                32,32
            );
        }
        
    

    }
    
    // Middle Ground
    for (let tile of viewMap[1]) {
        if (tile.active
            && tile.x *32 > c.x - 32 && tile.x * 32 < c.x + viewWidth * 32 
            && tile.y * 32 > c.y  - 32 && tile.y * 32 < c.y + viewHeight * 32)  {
            ctx.drawImage(
                tileSet,
                tile.getSprite().x,
                tile.getSprite().y,
                tile.getSprite().w,
                tile.getSprite().h,
                tile.getX() * 32 - c.x,
                tile.getY() * 32 - c.y,
                32,32);
        }
    }

    // Foreground

    // Info Display
    ctx.fillStyle = '#efd8a1';
    //ctx.drawImage(tileSet, 32, 0, 8, 8, 900, 10, 40,40);
    ctx.textAlign = "right";
	ctx.fillText(p.score, 1100, 20);
}