
class World {
    constructor(w,h) {
        this.levelOne = randomLevel(w, h);
    }
}

function randomLevel(w, h) {
    let level = new Array()
    let map = new Array();
    let item = new Array();
    let back = new Array();
    for (let i = 0; i < w * (h -1); i++) {
        let alea = Math.random()
        if (alea < 0.1) {
            map.push(new Object( i % w, Math.floor(i / w), SpriteInfo.GreenGroundMiddle, false));
            // Some flower ?
            if (Math.random() < 0.5) {
                let flowerType = randomInt(1, 7);
                switch (flowerType) {
                    case 1:
                        item.push(new Object( i % w, Math.floor(i / w) - 1, SpriteInfo.BlueFlower, true));
                    break;
                    case 2: 
                        item.push(new Object( i % w, Math.floor(i / w) - 1, SpriteInfo.WhiteFlower, true));
                    break;
                    case 3: 
                        item.push(new Object( i % w, Math.floor(i / w) - 1, SpriteInfo.RedFlower, true));
                    break;
                    case 4: 
                        item.push(new Object( i % w, Math.floor(i / w) - 1, SpriteInfo.GrassOne, true));
                    break;
                    case 5: 
                        item.push(new Object( i % w, Math.floor(i / w) - 1, SpriteInfo.GrassTwo, true));
                    break;
                    case 6: 
                        item.push(new Object( i % w, Math.floor(i / w) - 1, SpriteInfo.GrassThree, true));
                    break;
                    case 7:
                        item.push(new Object( i % w, Math.floor(i / w) - 1, SpriteInfo.RedMushroom, true));
                    break;
                }
            }
        } else {
            // if no tile, an object
            let aleb = Math.random();
            if(aleb < 0.05) {
                item.push(new Object(i % w, Math.floor(i / w), SpriteInfo.GoldCoin, true, true));
            }
        }
    }
    for (let i = (h-1) * w; i < w * h; i++) {
        map.push(new Object( i % w, Math.floor(i / w), SpriteInfo.GreenGroundMiddle, false))
    }
    level.push(back);
    level.push(map);
    level.push(item);
    

    return level;

}

function randomInt(min, max) {
    return Math.round((max - min) * Math.random() + min);
}