const EntityType = {
    PLAYER: 'player',
    MONSTER: 'monster',
}

class Entity {
    constructor(x, y, type) {
        this.x = x;
        this.y = y;
        this.w = 32;
        this.h = 32;
        this.type = type;
        this.sprite;
        this.goLeft = false;
        this.goRight = false;
        this.jump = false;
        this.bring = false;
        this.on_the_ground = false;
        this.sight = true;      //true for left, false for right
    }
    getX() {
        return this.x;
    }
    getY() {
        return this.y;
    }
    getType() {
        return this.type;
    }
    getW() {
        return this.w;
    }
    getH() {
        return this.h;
    }
    getSprite() {
        return this.sprite;
    }
    goingLeft(state) {
        this.goLeft = state;
    }
    goingRight(state) {
        this.goRight = state;
    }
    jumping(state) {
        this.jump = state;
    }

}

class Player extends Entity {
    constructor(x, y) {
        super(x, y, EntityType.PLAYER);
        this.sprite = SpriteInfo.PlayerJumpLeft;
        this.vx = 0;
        this.vy = 0;
        this.vxMax = 3;
        this.vyMax = -8;
        this.score = 0;

    }

    update(viewMap) {

        if (this.jump && this.on_the_ground) {
            this.vy = this.vyMax;
            //this.y -= 1
            this.on_the_ground = false;
        }

        // Set the good animation with the good movement
        if (this.goLeft && this.on_the_ground) {
            this.sprite = SpriteInfo.PlayerGoLeft;
            this.vx = -this.vxMax;
            this.sight = true;

        } else if (this.goRight && this.on_the_ground) {
            this.sprite = SpriteInfo.PlayerGoRight;
            this.vx = this.vxMax;
            this.sight = false;

        } else if (this.goLeft && !this.on_the_ground) {
            this.sight = true;
            this.vx = -this.vxMax;

            this.sprite = SpriteInfo.PlayerJumpLeft;

        }   else if (this.goRight && !this.on_the_ground) {
            this.sight = false;
            this.vx = this.vxMax;

            this.sprite = SpriteInfo.PlayerJumpRight;

        } else if (!this.goRight && !this.goLeft && !this.on_the_ground) {
            this.vx *= 0.95;
            if (this.sight) {
                this.sprite = SpriteInfo.PlayerJumpLeft;
            } else {
                this.sprite = SpriteInfo.PlayerJumpRight;
            }     
        } else if (!this.goLeft && !this.goRight && this.on_the_ground) {
            this.vx *= 0.7;
            if  (this.sight) {
                this.sprite = SpriteInfo.PlayerIdleLeft;
            } else {
                this.sprite = SpriteInfo.PlayerIdleRight;
            }
        }        

        let boxTop = {x: this.x + 2, y: this.y + this.vy, w: this.w - 4, h: this.h /2};
        let boxBottom = {x: this.x + 2, y: this.y + this.vy +1+ this.h/2, w: this.w - 4, h: this.h /2};
        let boxLeft = {x: this.x + this.vx, y: this.y + 4, w: this.w / 2, h: this.h - 8};
        let boxRight = {x: this.x + this.vx + this.w / 2, y: this.y + 4, w: this.w / 2, h: this.h - 8};

        this.on_the_ground = false;
        for (let tile of viewMap) {
            if (!tile.cross) {
                let tileBox = {x: tile.getX() * 32, y: tile.getY() * 32, w: 32, h: 32};

                if (is_intersect_rect(boxTop, tileBox)) {
                    this.y = tile.y * 32 + 32 ;
                    this.vy = 0;
                }
    
                if (is_intersect_rect(boxLeft, tileBox)) {
                    this.x = tile.x * 32 + 32;
                    this.vx = 0;
    
                }
    
                if (is_intersect_rect(boxRight, tileBox)) {
                    this.x = tile.x * 32 - this.w;
                    this.vx = 0;
    
                }
    
                if (is_intersect_rect(boxBottom, tileBox)) {
                    this.y = tile.y * 32 - this.h;
                    this.vy = 0;
                    this.on_the_ground = true;
                }
    
            }
        }
        this.vy += 0.2;
        
        this.x += this.vx;
        this.y += this.vy;
    }
}


class Object {
    constructor(x, y, sprite, cross, t = false) {
        this.x = x;
        this.y = y;
        this.w = 32;
        this.h = 32;
        this.cross = cross;
        this.active = true;
        this.take = true;
        this.autotake = t;
        this.sprite = sprite;

        // Adjust some object properties
        switch (this.sprite) {
            case SpriteInfo.GoldCoin :
                this.h = 28;
                this.w = 16;
            break;
            case SpriteInfo.BlueFlower:
                this.w = 16;
            break;
            case SpriteInfo.WhiteFlower:
                this.w = 16;
            break;
            case SpriteInfo.RedFlower:
                this.w = 16;
            break;
            case SpriteInfo.GrassOne:
                this.w = 16;
            break;
            case SpriteInfo.GrassTwo:
                this.w = 16;
            break;
            case SpriteInfo.GrassThree:
                this.w = 16;
            break;
            case SpriteInfo.RedMushroom:
                this.w = 20;
                this.h = 16;
            break;
        }
    }

    getX() {
        return this.x;
    }
    getY() {
        return this.y;
    }
    getW() {
        return this.w;
    }
    getH() {
        return this.h;
    }
    getSprite() {
        return this.sprite;
    }
}

function is_intersect_rect(rect1, rect2) {
    if (rect1.x < rect2.x + rect2.w &&
        rect1.x + rect1.w > rect2.x &&
        rect1.y < rect2.y + rect2.h &&
        rect1.h + rect1.y > rect2.y) {
        return true;
     } else {
         return false;
     }

}

function world_interaction(p, level) {
    // With object
    for (item of level[2]) {
        if (item.autotake || (item.take && p.bring)) {
            let pBox = {x: p.getX() , y: p.getY(), w: p.getW(), h: p.getH()}
            let tBox = {x: item.getX() * 32 + (32 - item.getW())/2, y: item.getY() *32 + (32 - item.getH()), w: item.getW(), h: item.getH()}
            if (is_intersect_rect(pBox, tBox)) {
                p.score += 1;
                item.active = false;
            }    
        }
    }
}

function entityCleaner(level) {         // Remobe inactive entity

    // Item Cleaner
    let i = 0;
    while (i < level[2].length) {
        if (!level[2][i].active) {
            level[2].splice(i, 1);
        } else {
            i += 1;
        }
    }

    // Some other stuff to clean ?
}